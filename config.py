from .utils import JSONFriendly
from .preprocess.config import PreprocessingConfig
from .models.ae.trainer import AETrainingConfig
from .models.classifier.trainer import ClassifierTrainingConfig
from .models.lstm.trainer import LSTMTrainingConfig


class Config(JSONFriendly):
    def __init__(self, file_name: str = None):
        super().__init__()
        self.ae = AETrainingConfig()
        self.lstm = LSTMTrainingConfig()
        self.classifier = ClassifierTrainingConfig()
        self.preprocessing = PreprocessingConfig()
        self.data_dir = "data"
        if file_name is not None:
            self.load_json(file_name)

    def load_json(self, file_name: str):
        super().load_json(file_name)
        preprocessing_config = PreprocessingConfig()
        preprocessing_config.load_dict(self.preprocessing)
        self.preprocessing = preprocessing_config
