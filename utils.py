import ipdb
import numpy as np
import torch as t
from pprint import pprint
import os, shutil
import json


def rmdir(folder: str):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print("Failed to delete %s. Reason: %s" % (file_path, e))


class JSONFriendly:
    def to_dict(self):
        result = {}
        for key, val in self.__dict__.items():
            if not key.startswith("-"):
                result[key] = (
                    val if not isinstance(val, JSONFriendly) else val.to_dict()
                )
        return result

    def load_dict(self, dictionary: dict):
        for key, val in dictionary.items():
            self.__dict__[key] = val

    def load_json(self, file_name: str):
        if os.path.exists(file_name):
            with open(file_name) as f:
                dictionary = json.load(f)
            for key, val in dictionary.items():
                self.__dict__[key] = val

    def write_json(self, file_name: str):
        values = self.to_dict()
        json.dump(
            values,
            open(os.path.join(file_name), "w"),
            indent=4,
        )

    def __str__(self):
        return json.dumps(self.to_dict(), indent=4)


def main():
    arr = np.array((4, 18, 10, 15, 1))
    print(arr)
    # print(len(arr))
    # np.random.shuffle(arr)
    mask = get_combos_mask(arr.tolist(), 24)
    print(arr[mask])


if __name__ == "__main__":
    main()
