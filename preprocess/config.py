from ..utils import JSONFriendly


class PreprocessingConfig(JSONFriendly):
    def __init__(self):
        self.data_directories = ["data"]
        self.preictal_window_duration = 2.0
        self.segment_duration = 5
        self.filter_frequencies_by = 700
        super().__init__()
