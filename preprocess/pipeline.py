from typing import Any, Callable, Iterable
import os
import json
import pickle
from tqdm import tqdm
import ipdb
from shutil import rmtree


class ComputeStage:
    def __init__(
        self,
        stage_func: Callable[[Any], Any],
        *,
        name: str = "",
        description: str = "",
        force: bool = False,
    ):
        self.force = force
        self.stage_func = stage_func
        self.name = name if name != "" else stage_func.__name__
        self.description = description

    def __str__(self):
        return f"{self.name} force={self.force}"

    def __call__(self, input: Any) -> Any:
        return self.stage_func(input)

    def save(self, data: Any, location: str):
        rmtree(location)
        os.mkdir(location)
        if isinstance(data, Iterable):
            data = list(data)
            sub_path = os.path.join(location, "list")
            if not os.path.exists(sub_path):
                os.mkdir(sub_path)
            for i, datum in tqdm(enumerate(data)):
                with open(os.path.join(sub_path, f"{i}.pickle"), "wb") as f:
                    pickle.dump(datum, f)
        else:
            with open(os.path.join(location, f"self.pickle"), "wb") as f:
                pickle.dump(data, f)

    def load(self, location: str) -> Any:
        result: Any = None
        if os.path.exists(os.path.join(location, "self.pickle")):
            with open(os.path.join(location, "self.pickle"), "rb") as f:
                result = pickle.load(f)
        else:
            sub_dir = os.path.join(location, "list")
            assert os.path.exists(sub_dir), (
                f"No data found in {location} \n"
                + "could not load cached output. You might want"
                + "to force execution of the previous stage"
            )
            assert os.path.exists(
                os.path.join(sub_dir, "0.pickle")
            ), "Directory 'list' is empty!!"
            result = []
            i = 0
            done = False
            while not done:
                file_location = os.path.join(sub_dir, f"{i}.pickle")
                if not os.path.exists(file_location):
                    done = True
                else:
                    with open(file_location, "rb") as f:
                        result.append(pickle.load(f))
                i += 1
        return result


class Pipeline:
    def __init__(
        self,
        stages: list[ComputeStage],
        storage_dir: str,
        force_all: bool = False,
    ):
        self.stages = stages
        if force_all:
            for stage in self.stages:
                stage.force = True
        self.storage_dir = storage_dir
        if not os.path.exists(self.storage_dir):
            os.mkdir(self.storage_dir)

    def __str__(self):
        out = "\nPipeline summary:\n"
        for i, stage in enumerate(self.stages):
            out += f"\t{i} {stage}\n"
        return out + "\n"

    def __call__(self, initial_input: Any):
        data: Any = initial_input
        skipped_last = False
        for i, stage in enumerate(self.stages):
            stage_location = os.path.join(self.storage_dir, f"stage_{i}")
            if not os.path.exists(stage_location):
                os.mkdir(stage_location)
            if not stage.force:
                print(f"SKIPPING stage {i};")
                skipped_last = True
            else:
                print(f"\n\nEXECUTING stage {i};")
                if skipped_last:
                    print("\nLoading previous stage output..")
                    data = self.stages[i - 1].load(
                        os.path.join(self.storage_dir, f"stage_{i-1}")
                    )
                    print("Done loading.")
                data = stage(data)
                print(f"\n\nDONE executing stage {i}")
                print("Saving stage output...")
                stage.save(data, stage_location)
                print("Done saving.")
                skipped_last = False
