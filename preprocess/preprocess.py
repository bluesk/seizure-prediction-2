import os
from argparse import ArgumentParser
import warnings
from pprint import pprint
import ipdb
import torch as t
from tqdm import tqdm
import pickle
import json

from .chbmit_summary import CHBMITSummary
from .conegliano_summary import ConeglianoSummary
from .summary import Summary, Recording
from .pipeline import Pipeline, ComputeStage
from ..config import Config
from .config import PreprocessingConfig
from .output_classes import Segment, PreprocessedData
from .mat_to_pickle import convert_to_pickle_if_necessary


def get_segment_and_save_with_config(
    output_dir: str, config: PreprocessingConfig
):
    def segment_and_save(summaries: list[Summary]):
        summaries = [su for su in summaries if len(su.recordings) > 0]
        sampling_frequency = summaries[0].sampling_frequency
        subsegment_length = int(
            summaries[0].sampling_frequency * config.segment_duration
        )
        means = t.tensor([])
        vars = t.tensor([])
        segments_count = t.tensor(0).long()
        n_seizures = 0
        subjects: set[str] = set()
        print("Computing mean and variance")
        for i, summary in tqdm(tuple(enumerate(summaries))):
            n_seizures += summary.n_seizures
            continuous_intervals = summary.get_continuous_intervals()
            if len(continuous_intervals) != 0:
                continuous_intervals = [
                    (label, signal)
                    for label, signal in continuous_intervals
                    if signal.shape[1] > subsegment_length
                ]
                new_means = t.stack(
                    tuple(t.mean(c[1], dim=1) for c in continuous_intervals)
                )
                new_vars = t.stack(
                    tuple(t.var(c[1], dim=1) for c in continuous_intervals)
                )
                means = t.cat((means, new_means))
                vars = t.cat((vars, new_vars))
                segments_count += len(continuous_intervals)
                subjects.add(summary.subject_name)
        means = means.T
        vars = vars.T
        total_mean = t.mean(means, dim=1)
        total_var = t.mean(vars, dim=1)
        segment_lengths = t.zeros(segments_count)
        j = 0
        shape = None
        non_preictal_subsegments_count = 0
        subsegments_count = 0
        files = []
        print("Generating segments")
        segments = []
        for summary in tqdm(summaries):
            continuous_intervals = summary.get_continuous_intervals()
            if len(continuous_intervals) > 0:
                continuous_intervals = [
                    (label, signal)
                    for label, signal in continuous_intervals
                    if signal.shape[1] > subsegment_length
                ]
                for label, signal in continuous_intervals:
                    signal = t.divide(signal.T - total_mean, total_var).T
                    new_segment = Segment(
                        signal,
                        label,
                        config.segment_duration,
                        sampling_frequency,
                        summary.subject_name,
                        config.filter_frequencies_by,
                    )
                    subsegments_count += len(new_segment.labels)
                    non_preictal_subsegments_count += t.sum(
                        new_segment.labels == 1.0
                    )
                    if (
                        len(segments) > 0
                        and new_segment.subject_name
                        != segments[0].subject_name
                    ) or len(segments) > 3:
                        file = os.path.join(
                            output_dir,
                            f"{len(files)}_{segments[0].subject_name}",
                        )
                        print(f"{file=}")
                        with open(file, "wb") as f:
                            pickle.dump(segments, f)
                        files.append(file)
                        segments = []

                    segments.append(new_segment)
                    if shape == None:
                        shape = segments[0].signals.shape[1:]
                    segment_lengths[j] = signal.shape[1]
                j += 1
        file = os.path.join(output_dir, f"segment_batch_{len(files)}")
        print(f"{file=}")
        with open(file, "wb") as f:
            pickle.dump(segments, f)
        files.append(file)
        preprocessed_data = PreprocessedData(
            subsegments_count=int(subsegments_count),
            non_preictal_count=int(non_preictal_subsegments_count),
            segment_count=int(segments_count),
            subjects=list(subjects),
            sampling_frequency=summaries[0].sampling_frequency,
            window_seconds=config.segment_duration,
            shape=[int(s) for s in shape],
            n_seizures=int(n_seizures),
            storage_dir=output_dir,
            mean_segment_duration=float(t.mean(segment_lengths)),
            var_segment_duration=float(t.var(segment_lengths)),
        )
        print(
            f"{preprocessed_data.non_prectal_count/preprocessed_data.subsegments_count=}"
        )
        preprocessed_data.write_json()
        print(preprocessed_data)

    return segment_and_save


def select_channels(summaries: list[Summary]):
    all_channels: set[str] = set()
    for summary in summaries:
        # print(tuple(s.split('-') for s in summary.channels))
        c = tuple(s.upper() for s in summary.channels)
        all_channels.add(
            tuple(
                s
                for s in c
                if "REF" not in s
                and (
                    1 < len(s) < 3
                    or (
                        "-" in s
                        and len(
                            tuple(
                                a
                                for a in s.strip(" ").split("-")
                                if len(a) == 0
                            )
                        )
                        == 0
                    )
                )
            )
        )
    unique_channels: set[str] = set()
    for el in list(all_channels):
        unique_channels.update(el)
    unique_channels = list(unique_channels)
    conegliano_channels = tuple(c for c in unique_channels if len(c) == 2)

    def compatible(a: str, b: str):
        return False if len(a) == len(b) else (a in b or b in a)

    compatibles = [
        (c, tuple(a for a in unique_channels if compatible(a, c)))
        for c in conegliano_channels
    ]
    couples: list[tuple[str, str]] = []
    while len(compatibles) > 0:
        combo = compatibles[0]
        if len(combo[1]) > 0:
            couples.append((combo[0], combo[1][0]))
            next_compatible = [
                (a, tuple(b for b in c if b != combo[1][0]))
                for a, c in compatibles[1:]
            ]
            compatibles = next_compatible
        else:
            compatibles = compatibles[1:]

    frequencies = t.zeros(len(couples))
    for i, (c1, c2) in enumerate(couples):
        for su in summaries:
            if c1 in su.channels or c2 in su.channels:
                frequencies[i] += 1
    max_freq = t.max(frequencies)
    couples = [c for i, c in enumerate(couples) if frequencies[i] == max_freq]
    return couples


def get_load_summaries(config: PreprocessingConfig):
    def load_summaries(directories: list[str]):
        all_paths: list[str] = []
        for dir in directories:
            for subdir in os.listdir(dir):
                if os.path.isdir(os.path.join(dir, subdir)):
                    all_paths.append(os.path.join(dir, subdir))

        print("Generating summaries")
        summaries: list[Summary] = []
        for dir in tqdm(all_paths):
            if ConeglianoSummary.is_dataset(dir):
                summaries.extend(
                    ConeglianoSummary.from_folder(
                        dir, config.preictal_window_duration
                    )
                )
            elif CHBMITSummary.is_dataset(dir):
                summaries.extend(
                    CHBMITSummary.from_folder(
                        dir, config.preictal_window_duration
                    )
                )
        print("Making channels uniform")
        couples = select_channels(summaries)
        for summary in summaries:
            summary.apply_channels_mask(couples)
        summaries = [s for s in summaries if len(s.channels) == len(couples)]
        print(f"{couples=}")
        print(f"{len(summaries)=}")
        return summaries

    return load_summaries


def preprocess(config_location: str, global_config: Config):
    output_dir = global_config.data_dir
    config = global_config.preprocessing
    convert_to_pickle_if_necessary(config_location, global_config)

    cache_dir = os.path.join(output_dir, ".cache")
    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    load_summaries = get_load_summaries(config)
    segment_and_save = get_segment_and_save_with_config(
        output_dir=output_dir, config=config
    )
    pipeline = Pipeline(
        [
            ComputeStage(load_summaries, force=True),
            ComputeStage(segment_and_save, force=True),
        ],
        cache_dir,
    )
    print(pipeline)
    pipeline(config.data_directories)
