import os
import torch as t
from datetime import datetime, timedelta
import json
from tqdm import tqdm
import mne
import ipdb
import warnings
from .summary import Summary, Recording


class CHBMITRecording(Recording):
    @property
    def signal(self) -> t.Tensor:
        warnings.filterwarnings("ignore")
        signal = t.from_numpy(
            mne.io.read_raw_edf(self.file_name, verbose=False).get_data()
        )
        warnings.filterwarnings("default")
        return signal


class ProtoRecordingEvent:
    def __init__(
        self,
        file_name: str,
        start_time: datetime,
        end_time: datetime,
        seizures: tuple[tuple[int, int], ...],
    ):
        self.file_name = file_name
        self.start_time = start_time
        self.end_time = end_time
        self.seizures = seizures

    def __str__(self) -> str:
        return json.dumps(self.__dict__, default=str)


class CHBMITSummary(Summary):
    def __init__(
        self, raw_summary_location: str, preictal_window_hours: float
    ):
        subject_name = os.path.basename(raw_summary_location).strip(
            "-summary.txt"
        )
        print(f"Generating summary for patient: {subject_name}")
        with open(raw_summary_location) as f:
            content = f.read().split("\n\n")
        channels_changed_location = tuple(
            i for i, line in enumerate(content) if "Channels changed" in line
        )
        content = content[
            : channels_changed_location[0]
            if len(channels_changed_location) > 0
            else len(content)
        ]
        sampling_frequency = 256
        super().__init__(
            CHBMITSummary.__get_channels(content),
            CHBMITSummary.__get_recording_events(
                os.path.dirname(raw_summary_location),
                content,
                sampling_frequency,
            ),
            subject_name,
            sampling_frequency,
            preictal_window_hours,
        )

    @staticmethod
    def __validate(summary_location: str) -> bool:
        text = open(summary_location).read()
        return "File Start Time" in text

    @staticmethod
    def is_dataset(folder: str) -> bool:
        summary_location = os.path.join(
            folder, f"{os.path.basename(folder)}-summary.txt"
        )
        return os.path.exists(summary_location)

    @staticmethod
    def from_folder(
        folder: str, preictal_window_hours: float
    ) -> list[Summary]:
        name = os.path.basename(folder)
        summary_location = os.path.join(folder, f"{name}-summary.txt")
        return (
            [CHBMITSummary(summary_location, preictal_window_hours)]
            if CHBMITSummary.__validate(summary_location)
            else []
        )

    @staticmethod
    def __to_datetime(day: int, time: str) -> datetime:
        split = time.split(":")
        if len(split) < 3:
            ipdb.set_trace()
        h, m, s = tuple(int(val) for val in time.split(":"))
        return datetime(2000, 1, day, h if h < 24 else h - 24, m, s)

    @staticmethod
    def __get_channels(content: list[str]) -> tuple[str, ...]:
        start_idx = tuple(
            i for i, line in enumerate(content) if "Channels" in line
        )[0]
        return tuple(
            sorted(
                tuple(
                    line.split(":")[1].strip(" ").upper().replace(".", "-")
                    for line in content[start_idx].split("\n")[2:]
                )
            )
        )

    @staticmethod
    def __parse_recording_event(
        description: str, location: str
    ) -> ProtoRecordingEvent:
        lines = tuple(
            val
            for val in tuple(
                line.split(": ") for line in description.split("\n")
            )
            if len(val) >= 2
        )
        if len(lines[1][1]) == 1 or len(lines[2][1]) == 1:
            ipdb.set_trace()

        return ProtoRecordingEvent(
            os.path.join(location, lines[0][1]),
            CHBMITSummary.__to_datetime(1, lines[1][1]),
            CHBMITSummary.__to_datetime(1, lines[2][1]),
            (
                tuple(
                    (
                        int(lines[i][1].split()[0]),
                        int(lines[i + 1][1].split()[0]),
                    )
                    for i in range(4, len(lines) - 1, 2)
                )
                if len(lines) > 4
                else tuple()
            ),
        )

    @staticmethod
    def __get_recording_events(
        location: str, content: list[str], sampling_frequency: int
    ) -> tuple[Recording, ...]:
        recordings = tuple(
            CHBMITSummary.__parse_recording_event(item, location)
            for item in content
            if "File Name" in item
        )
        start_date = recordings[0].start_time
        days = timedelta(days=1)
        last_date = recordings[0].start_time
        fixed_recordings: list[Recording] = []
        for rec in tqdm(recordings[1:]):
            rec: ProtoRecordingEvent = rec
            cur_date = rec.start_time + days
            days += timedelta(days=int(cur_date < last_date))
            fixed_cur_start_date = rec.start_time + days
            proto_fixed_cur_end_date = rec.end_time + days
            days = (
                days
                if not fixed_cur_start_date > proto_fixed_cur_end_date
                else days + timedelta(days=1)
            )
            fixed_cur_end_date = rec.end_time + days

            fixed_cur_start_relative = int(
                (fixed_cur_start_date - start_date).total_seconds()
                * sampling_frequency
            )
            fixed_cur_end_relative = int(
                (fixed_cur_end_date - start_date).total_seconds()
                * sampling_frequency
            )
            last_date = rec.start_time + days
            cur_seizures = tuple(
                (
                    (
                        fixed_cur_start_relative
                        + int(s[0] * sampling_frequency)
                    ),
                    (
                        fixed_cur_start_relative
                        + int(s[1] * sampling_frequency)
                    ),
                )
                for s in rec.seizures
            )
            fixed_recordings.append(
                CHBMITRecording(
                    rec.file_name,
                    fixed_cur_start_relative,
                    fixed_cur_end_relative,
                    cur_seizures,
                )
            )
        return tuple(fixed_recordings)


def main():
    data_dir = "seizure_prediction/preprocess/test_data"
    for file_name in os.listdir(data_dir)[1:]:
        summary = CHBMITSummary(os.path.join(data_dir, file_name))
        print(summary.channels)


if __name__ == "__main__":
    main()
