import json
import torch as t
import os
import numpy as np
import ipdb

from ..utils import JSONFriendly


class PreprocessedData(JSONFriendly):
    def __init__(
        self,
        subsegments_count: int = 5,
        non_preictal_count: int = 1000,
        segment_count: int = 100,
        subjects: list[str] = [],
        sampling_frequency: int = 256,
        window_seconds: float = 5,
        shape: tuple[int, int, int] = (5, 5, 5),
        n_seizures: int = 15,
        storage_dir: str = "",
        mean_segment_duration: float = 100.0,
        var_segment_duration: float = 100.0,
    ):
        self.storage_dir = storage_dir
        self.subsegments_count = subsegments_count
        self.non_prectal_count = non_preictal_count
        self.segment_count = segment_count
        self.subjects = subjects
        self.sampling_frequency = sampling_frequency
        self.window_seconds = window_seconds
        self.n_seizures = n_seizures
        self.shape = shape
        self.mean_segment_duration = mean_segment_duration
        self.var_segment_duration = var_segment_duration
        self.files = [
            os.path.join(storage_dir, fn)
            for fn in os.listdir(storage_dir)
            if not fn.endswith(".json") and os.path.isfile(fn)
        ]
        file_name = os.path.join(self.storage_dir, "summary.json")
        if os.path.exists(file_name):
            self.load_json(file_name)

    def write_json(self):
        super().write_json(os.path.join(self.storage_dir, "summary.json"))


class Segment:
    def __init__(
        self,
        signal: t.Tensor,
        label: t.Tensor,
        segment_duration: int,
        sampling_frequency: int,
        subject_name: str,
        filter_frequencies_by: int,
    ):
        self.subject_name = subject_name
        self.sampling_frequency = sampling_frequency
        self.filter_frequencies_by = filter_frequencies_by
        subsegment_length = int(
            np.floor(segment_duration * sampling_frequency)
        )
        fixed_length: int = subsegment_length * (
            len(label) // subsegment_length
        )
        fixed_signal = signal[:, :fixed_length]
        fixed_label = label[:fixed_length]
        signal_chunks = fixed_signal.reshape(
            (-1, signal.shape[0], subsegment_length)
        )
        dense_labels = fixed_label.view((-1, subsegment_length))
        self.labels = t.mean(dense_labels, dim=1)
        self.signals = t.stack(
            tuple(
                self.__do_stft_single(chunk).transpose(0, 1)
                for chunk in signal_chunks
            )
        )

    def __do_stft_single(self, signal: t.Tensor, delay: float = 0.88):
        delay_size = int(delay * self.sampling_frequency)
        window_size = 2 * delay_size
        segments = tuple(
            signal[:, i * delay_size : i * delay_size + window_size]
            for i in range(
                signal.shape[1] // delay_size
                - int(np.ceil(delay_size / window_size))
            )
        )
        fft_segments = t.stack(
            tuple(
                t.fft.rfft(segment, dim=1)[:, : self.filter_frequencies_by]
                for segment in segments
            ),
        )
        return t.view_as_real(fft_segments)
