from genericpath import isdir
import scipy.io
import pickle
import os
import shutil
from ..config import Config
import sys


def is_conegliano_and_pickle(path: str):
    sublocations = [os.path.join(path, d) for d in os.listdir(path)]
    a_subdir = [d for d in sublocations if os.path.isdir(d)][0]
    return os.path.exists(os.path.join(a_subdir, "Channels.mat"))


def convert_to_pickle_if_necessary(config_location: str, config: Config):
    data_directories = config.preprocessing.data_directories.copy()
    for i, data_dir in enumerate(data_directories):
        if is_conegliano_and_pickle(data_dir):
            answer = None
            while answer not in ["yes", "no"]:
                answer = input(
                    "Conegliano dataset in MAT format found. Convert it now? (yes/no) \n"
                )
            if answer == "yes":
                pickle_name = f"{data_dir}_pickle"
                mat_to_pickle(data_dir, pickle_name)
                print(f"Converted dataset to pickle, location: {pickle_name}")
                print("Updating configuration file..")
                config.preprocessing.data_directories[i] = pickle_name
                config.write_json(config_location)
            else:
                print("OK, exiting.")
                sys.exit()


def mat_to_pickle(in_dir: str, out_dir: str):
    print(f"Converting location {in_dir} <--> {out_dir}")
    if not os.path.exists(out_dir):
        os.mkdir(out_dir)

    sub_locations = os.listdir(in_dir)
    for sub_location in sub_locations:
        full_sub_in_location = os.path.join(in_dir, sub_location)
        full_sub_out_location = os.path.join(out_dir, sub_location)
        if os.path.isdir(full_sub_in_location):
            os.mkdir(full_sub_out_location)
            mat_to_pickle(full_sub_in_location, full_sub_out_location)
        else:
            if sub_location.endswith(".mat"):
                data = scipy.io.loadmat(full_sub_in_location)
                out_name = full_sub_out_location[:-4]
                with open(out_name, "wb") as f:
                    pickle.dump(data, f)
            else:
                shutil.copyfile(full_sub_in_location, full_sub_out_location)


if __name__ == "__main__":
    mat_to_pickle("evo/conegliano", "evo/conegliano_pickle")
