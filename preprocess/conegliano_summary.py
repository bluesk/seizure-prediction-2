import os
from typing import Any, Iterable

# import scipy.io
import numpy as np
from .summary import Recording, Summary
import mne
import torch as t
import ipdb
import pickle


class ConeglianoRecording(Recording):
    @property
    def signal(self) -> t.Tensor:
        with open(self.file_name, "rb") as f:
            data = pickle.load(f)
        return t.from_numpy(data["Data"])


class ConeglianoSummary(Summary):
    def __init__(self, data_dir: str, id: int, preictal_window_hours: float):
        subject_name = os.path.basename(data_dir)
        print(f"Generating summary from: {subject_name}, Recording: {id}")
        sampling_frequency = 256

        channels = ConeglianoSummary.__get_channels(data_dir)

        super(ConeglianoSummary, self).__init__(
            channels,
            ConeglianoSummary.__get_recording_events(
                data_dir, id, sampling_frequency
            ),
            subject_name,
            sampling_frequency,
            preictal_window_hours,
        )

    @staticmethod
    def is_dataset(folder: str) -> bool:
        return os.path.exists(os.path.join(folder, "Channels"))

    @classmethod
    def from_folder(cls, folder: str, preictal_window_hours: float):
        recording_ids: list[int] = sorted(
            list(
                set(
                    [
                        int(fn.split("block")[0].split("_")[1])
                        for fn in os.listdir(os.path.join(folder, "Events"))
                    ]
                )
            )
        )
        return [cls(folder, id, preictal_window_hours) for id in recording_ids]

    @staticmethod
    def __get_channels(data_dir: str) -> tuple[str, ...]:
        # raw_file = scipy.io.loadmat(os.path.join(data_dir, "Channels.mat"))
        path = os.path.join(data_dir, "Channels")
        with open(path, "rb") as f:
            raw_data = pickle.load(f)
        print(f"Reading file Channels")
        raw_data = (
            raw_data["Channels"]
            if "Channels" in raw_data
            else raw_data["Channel"]
        )
        raw_channels: tuple[str, ...] = tuple(
            c[0][0].upper() for c in raw_data.squeeze().tolist()
        )
        return tuple(sorted(raw_channels))

    @staticmethod
    def __get_events(event_files: list[str]):
        all_events = []
        for event_fname in event_files:
            print(f"Reading file: {event_fname}")
            with open(event_fname, "rb") as f:
                raw_events = pickle.load(f)["Events"].squeeze().tolist()
            # raw_events = (
            #    scipy.io.loadmat(event_fname)["Events"].squeeze().tolist()
            # )
            events = []
            for re in raw_events:
                if type(re) == tuple:
                    events.append([re[0][0].upper(), re[3][0]])
            all_events.append(events)
        result = []
        for event in all_events:
            all = {}
            for e, times in event:
                for i, t in enumerate(times):
                    if i not in all:
                        all[i] = []
                    all[i].append((e, t))
            result.append(
                [sorted(a, key=lambda x: x[1]) for a in all.values()]
            )

        return result

    @staticmethod
    def __get_recording_events(
        data_dir: str, id: int, sampling_frequency: int
    ) -> tuple[ConeglianoRecording, ...]:
        events_dir = os.path.join(data_dir, "Events")
        signals_dir = os.path.join(data_dir, "EEG_matrix")
        event_files = sorted(
            [
                os.path.join(events_dir, fn)
                for fn in os.listdir(events_dir)
                if str(id) in fn
            ],
            key=lambda x: int(x.split("block")[1].split(".")[0]),
        )
        signal_files = sorted(
            [
                os.path.join(signals_dir, fn)
                for fn in os.listdir(signals_dir)
                if str(id) in fn
            ],
            key=lambda x: int(x.split("block")[1].split(".")[0]),
        )
        events = ConeglianoSummary.__get_events(event_files)

        recordings: list[ConeglianoRecording] = []
        current_time = 0
        for i, recording_fname in enumerate(signal_files):
            print(f"Reading file name {recording_fname}")
            with open(recording_fname, "rb") as f:
                duration = pickle.load(f)["Data"].shape[1]
            # duration = scipy.io.loadmat(recording_fname)["Data"].shape[1]
            seizures = [
                (
                    int(sampling_frequency * e[0][1]),
                    int(sampling_frequency * e[-1][1]),
                )
                for e in events[i]
            ]
            recording = ConeglianoRecording(
                recording_fname,
                current_time,
                current_time + duration,
                seizures,
            )
            current_time += duration
            recordings.append(recording)
        return tuple(recordings)


def main():
    folder = "/home/bluesk/documents/hdd/new_data/Dati_per_Alberto/Bog_Giu"
    print(f"{ConeglianoSummary.is_dataset(folder)=}")
    summaries = ConeglianoSummary.from_folder(folder)
    print(summaries[0].channels)
    print(len(summaries))

    # print(summary)


if __name__ == "__main__":
    main()
