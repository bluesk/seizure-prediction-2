import json
from abc import ABC, abstractmethod
import torch as t
import matplotlib.pyplot as plt
import numpy as np
import torch as t
import ipdb


class Recording(ABC):
    def __init__(
        self,
        file_name: str,
        start_time: int,
        end_time: int,
        seizures: tuple[tuple[int, int], ...],
    ):
        self.file_name = file_name
        self.start_time = start_time
        self.end_time = end_time
        self.seizures = seizures
        self.labels: t.Tensor = t.zeros(self.end_time - self.start_time)
        self.ignore_mask = (t.zeros(len(self.labels)) + 1).bool()

    @property
    @abstractmethod
    def signal(self) -> t.Tensor:
        pass


class Summary(ABC):
    def __init__(
        self,
        channels: tuple[str, ...],
        recording_events: tuple[Recording, ...],
        subject_name: str,
        sampling_frequency: int,
        preictal_window_hours: float,
    ):
        self.subject_name = subject_name
        self.channels = channels
        self.recordings = recording_events
        self.sampling_frequency = sampling_frequency
        self.preictal_window_hours = preictal_window_hours
        self.__assign_labels()
        """
        label = t.cat(tuple(r.labels for r in self.recordings))
        signal = t.cat(tuple(r.get_signal() for r in self.recordings), dim=1)
        self.__compute_mean_and_var(label, signal)
        self.__compute_continuous_intervals(label)
        self.channels_mask = np.arange(len(channels))
        self.global_mean = t.tensor(0.0)
        self.global_var = t.tensor(1.0)
        ipdb.set_trace()
        """

    def __get_idx_end_continuities(self, labels: t.Tensor) -> t.Tensor:
        nonzero_idxs = t.where(labels != 0)[0]
        forward = nonzero_idxs[1:]
        backward = nonzero_idxs[:-1]
        discontinuities_idxs = t.where(t.abs((forward - backward)) > 1)[0] + 1
        """
        nonzero_idxs = t.where(labels != 0)[0]
        disconinuities_idxs = []
        for i in range(len(nonzero_idxs)):
            if i > 0:
                if nonzero_idxs[i] - nonzero_idxs[i-1] > 1:
                    disconinuities_idxs.append(i)
        """
        if len(nonzero_idxs) > 0:
            if labels[0] == 0:
                discontinuities_idxs = t.cat(
                    (t.tensor([nonzero_idxs[0]]), discontinuities_idxs)
                )
            else:
                discontinuities_idxs = t.cat(
                    (t.tensor([0]), discontinuities_idxs)
                )
            discontinuities_idxs = t.cat(
                (discontinuities_idxs, t.tensor([nonzero_idxs[-1]]))
            )
        # return t.tensor([disconinuities_idxs[:-1], disconinuities_idxs[1:]]).T

        return t.stack((discontinuities_idxs[:-1], discontinuities_idxs[1:])).T

    def get_continuous_intervals(self):
        result: list[tuple[t.Tensor, t.Tensor]] = []
        rec_i = 0
        for rec_i in range(len(self.recordings)):
            rec = self.recordings[rec_i]
            signal = rec.signal[:, rec.ignore_mask]
            labels = rec.labels[rec.ignore_mask]
            if len(labels) > 0:
                discontinuity_idxs = self.__get_idx_end_continuities(labels)
                # ipdb.set_trace()
                if (
                    len(result) > 0
                    and labels[0] != 0
                    and self.recordings[rec_i - 1].labels[-1] != 0
                    and (
                        self.recordings[rec_i].start_time
                        - self.recordings[rec_i - 1].end_time
                    )
                    <= 1
                ):
                    begin = t.tensor(0)
                    end: t.Tensor = discontinuity_idxs[0][1]
                    result[-1] = (
                        t.cat((result[-1][0], labels[begin:end])),
                        t.cat(
                            (
                                result[-1][1],
                                signal[self.channels_mask, begin:end],
                            ),
                            dim=1,
                        ),
                    )
                    discontinuity_idxs = discontinuity_idxs[1:]
                for begin, end in discontinuity_idxs:
                    adjusted = signal[self.channels_mask, begin:end]
                    result.append((labels[begin:end], adjusted))
        return result

    @property
    def n_seizures(self) -> int:
        return sum(len(rec.seizures) for rec in self.recordings)

    @classmethod
    def from_folder(cls, folder: str):
        dummy: list[cls] = []
        return dummy

    @staticmethod
    def is_dataset(folder: str) -> bool:
        return False

    """
    @property
    def signal(self) -> t.Tensor:
        signal = t.cat(tuple(r.signal for r in self.recordings), dim=1)
        return ((signal - self.global_mean) / self.global_var)[self.channels_mask][
            self.without_seizure_mask
        ]
    """

    def apply_channels_mask(self, couples: list[tuple[str, str]]):
        channels_map = {key: i for i, key in enumerate(self.channels)}
        self.channels_mask = []
        for c1, c2 in couples:
            if c1 in channels_map:
                self.channels_mask.append(channels_map[c1])
            elif c2 in channels_map:
                self.channels_mask.append(channels_map[c2])
        self.channels = np.array(self.channels)[self.channels_mask].tolist()
        print(f"{self.channels=}\n")
        print(f"{self.channels_mask=}")

    def __assign_labels(self):
        print("Assigning labels")
        max_label_value = (
            self.preictal_window_hours * 60 * 60 * self.sampling_frequency
        )
        last_seizure = (None, None)
        for rec in self.recordings[::-1]:
            if len(rec.seizures) > 0:
                last_seizure = rec.seizures[-1]
                break
        if not last_seizure == (None, None):
            for rec in self.recordings[::-1]:
                time = t.arange(rec.start_time, rec.end_time)
                rec.labels = last_seizure[0] - time
                rec.ignore_mask = (t.zeros(len(time)) + 1).bool()
                for last_seizure_event in rec.seizures:
                    last_seizure = last_seizure_event
                    mask = time < last_seizure[0]
                    length = len(rec.labels[mask])
                    rec.labels[mask] = last_seizure[0] - time[mask][:length]
                    rec.labels[
                        (time >= last_seizure[0]) & (time <= last_seizure[1])
                    ] = 0
                rec.ignore_mask[
                    (rec.labels < 0) & (time < max_label_value)
                ] = False
                rec.labels = (
                    t.clamp(
                        rec.labels,
                        max=max_label_value,
                        min=-1,
                    )
                    / max_label_value
                ).cpu()
                # rec.labels[rec.labels < 0] = 1.0
        else:
            for rec in self.recordings:
                time = t.arange(rec.start_time, rec.end_time)
                rec.ignore_mask = (t.zeros(len(time)) + 1).bool()
                rec.ignore_mask[time < max_label_value] = False
                rec.labels[time >= max_label_value] = 1.0

    def plot(self, index: int = -1):
        print("Started plotting")
        recording_events: list[Recording] = []
        """
        if index == -1:
            for rec in self.recordings:
                if len(rec.seizures) > 0:
                    recordings.append(rec)
                    break
        else:
            recordings.append(self.recordings[index])
        """
        recording_events = list(self.recordings[:10])
        print(f"{len(recording_events)=}")
        print(
            f"{recording_events[0].start_time=} {recording_events[-1].end_time=}"
        )
        time_diff = (
            recording_events[-1].end_time - recording_events[0].start_time
        )
        print(f"{time_diff=}")
        time = t.arange(
            recording_events[0].start_time, recording_events[-1].end_time
        )
        total_labels = t.zeros(time_diff) - 1

        print(len(time))
        print(len(total_labels))
        for rec in recording_events:
            mask = (time >= rec.start_time) & (time <= rec.end_time)
            # print(len(total_labels[mask]))
            total_labels[mask] = (
                t.cat((rec.labels, t.tensor([rec.labels[-1]])))
                if len(total_labels[mask]) == (len(rec.labels) + 1)
                else rec.labels
            )
        # print(total_labels.tolist())
        plt.plot(time, total_labels)
        for rec in recording_events:
            for seizure in rec.seizures:
                print(f"{seizure=}")
                plt.axvline(x=seizure[0], ymin=0, ymax=1)
                plt.axvline(x=seizure[1], ymin=0, ymax=1)
        plt.show()
        # plt.savefig("labels.png")

    def __str__(self):
        def to_json(x):
            if type(x) == t.Tensor:
                return x.__str__()
            return json.loads(
                json.dumps(x.__dict__, indent=4, default=to_json)
            )

        return json.dumps(self.__dict__, indent=4, default=to_json)
