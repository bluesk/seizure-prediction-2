import matplotlib.pyplot as plt
import numpy as np
from .data_loader import DataLoader


def main():
    data_loader = DataLoader("evo/seizure_prediction_1m_4h", 3, 3)
    data = None
    for batch in data_loader.train():
        data = batch.signals[0][0]
        break
    for i, ch in enumerate(data):
        plt.plot(ch + 10 * i, linewidth=0.5, color="black")
    plt.show()


if __name__ == "__main__":
    main()
