from tqdm import tqdm
import os

from torchinfo import summary as model_summary

import torch as t
from torch.optim.lr_scheduler import StepLR
from torch.utils.tensorboard import SummaryWriter

from ..utils import rmdir, JSONFriendly
from ..data_loader import DataLoader


class TrainingConfig(JSONFriendly):
    def __init__(self):
        self.lr = 1e-3
        self.saved_model = "saved_models/model.pt"
        self.epochs = 2
        self.gamma = 1.0
        self.train_ratio = 0.8
        self.train_batch_size = 30
        self.test_batch_size = 60


class Trainer:
    def __init__(self, *, model, criterion, data_dir, config: TrainingConfig):
        self.config = config
        self.device = t.device("cuda" if t.cuda.is_available() else "cpu")
        self.data_loader = DataLoader(
            data_dir,
            train_ratio=config.train_ratio,
            train_batch_size=config.train_batch_size,
            test_batch_size=config.test_batch_size,
        )
        self.model = model.to(self.device)
        self.criterion = criterion
        self.optimizer = t.optim.Adam(self.model.parameters(), lr=config.lr)
        self.scheduler = StepLR(
            self.optimizer, step_size=1, gamma=config.gamma
        )
        print(self.scheduler)
        if os.path.exists("runs"):
            rmdir("runs")
        self.writer = SummaryWriter()
        for batch in self.data_loader.train_loader:
            print(batch.signals.shape)
            model_summary(self.model, batch.signals.shape)
            break

    def train_started(self):
        pass

    def train_ended(self):
        pass

    def val_started(self):
        pass

    def val_ended(self):
        pass

    def test_started(self):
        pass

    def test_ended(self):
        pass

    def run(self, batch: tuple[t.Tensor, t.Tensor]) -> t.Tensor:
        return t.tensor(0.0)

    def train(self):
        # avg_test_loss = self.test()
        # print(f"Avg test loss: {round(avg_test_loss.item(), 3)}")
        # self.test_ended()
        # avg_test_loss = self.test()
        # print(f"Avg test loss: {round(avg_test_loss.item(), 3)}")
        # :w
        # self.test_ended()

        print("Starting training")
        batch_i = 1
        for epoch in range(1, self.epochs + 1):
            print("Epoch:", epoch)
            self.model.train()
            self.train_started()
            total_loss = t.tensor(0.0)
            train_dataset = self.data_loader.train_loader
            for batch in tqdm(train_dataset):
                self.optimizer.zero_grad()
                loss = self.run(batch)
                self.writer.add_scalar("train_loss", loss, global_step=batch_i)
                total_loss += loss.item()
                loss.backward()
                self.optimizer.step()
                if (batch_i % self.reduce_lr_every) == 0:
                    self.scheduler.step()
                    print("Reduced lr:", self.scheduler.get_last_lr())
                    for param_group in self.optimizer.param_groups:
                        print(param_group["lr"])
                batch_i += 1
            self.scheduler.step()
            avg_train_loss = total_loss / train_dataset.items_count
            print(f"Avg train loss: {round(avg_train_loss.item(), 6)}")
            self.writer.add_scalar(
                "avg_train_loss", avg_train_loss, global_step=epoch
            )
            self.train_ended()
            self.val_started()
            avg_val_loss = self.test(self.data_loader.val())
            self.val_ended()
            print(f"Avg val loss: {round(avg_val_loss.item(), 6)}")
            self.writer.add_scalar(
                "avg_val_loss", avg_val_loss, global_step=epoch
            )
        self.test_started()
        avg_test_loss = self.test()
        self.test_ended()
        print(f"Avg test loss: {round(avg_test_loss.item(), 3)}")
        self.writer.add_text("results", f"test_avg_loss {avg_test_loss}")

    def test(self, dataset=None):
        dataset = dataset if dataset is not None else self.data_loader.test()
        with t.no_grad():
            self.model.eval()
            total_loss = t.tensor(0.0)
            for batch in tqdm(dataset):
                # reconstructed = self.model(batch.signals)
                # loss = self.criterion(reconstructed, batch.signals)
                loss = self.run(batch)
                total_loss += loss.item()
        return total_loss / dataset.items_count

    def save(self):
        t.save(
            self.model.state_dict(),
            os.path.join(self.model_file_path, "model.pt"),
        )

    def plot_reconstruction(self):
        pass
