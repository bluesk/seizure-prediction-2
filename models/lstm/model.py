import torch as t
import torch.nn.functional as F
from torch import nn
from ..ae.model import ConvEncoder
import ipdb


class ConvLSTM(nn.Module):
    def __init__(self, encoder_path: str, dropout: float = 0.2):
        super().__init__()
        embedding_dim = 19
        seq_length = 135
        self.embedding_dim = embedding_dim
        self.seq_length = seq_length
        self.encoder = ConvEncoder(dropout)
        self.encoder.load_state_dict(t.load(encoder_path))
        self.lstm = nn.LSTM(
            input_size=embedding_dim,
            hidden_size=seq_length,
            num_layers=1,
            bidirectional=True,
        )
        self.dropout = nn.Dropout()
        self.fc = nn.Linear(2 * self.seq_length, 2)

    def forward(self, x: t.Tensor) -> t.Tensor:
        self.encoder.eval()
        x = self.encoder(x)
        x = x.view(-1, self.seq_length, self.embedding_dim)
        output, _ = self.lstm(x)
        out_forward = output[
            range(len(output)), self.seq_length - 1, : self.embedding_dim
        ]
        out_reverse = output[:, 0, self.embedding_dim :]
        out_reduced = self.dropout(
            t.cat((out_forward, out_reverse), 1)
        ).squeeze(1)
        x = F.relu(self.fc(out_reduced))
        x = F.log_softmax(x, dim=1)
        return x
