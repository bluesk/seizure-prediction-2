import os
import ipdb
import torch as t
import torch.nn.functional as F

from ...utils import JSONFriendly
from ...data_loader import DataLoader
from ..trainer import Trainer
from .model import ConvLSTM


class LSTMTrainingConfig(JSONFriendly):
    def __init__(self):
        super().__init__()


class ConvLSTMTrainer(Trainer):
    def __init__(self, data_dir: str, config: LSTMTrainingConfig):
        gamma = gamma if gamma != 0 else 0.85
        super().__init__(
            model=ConvLSTM(os.path.join(model_file_path, "encoder.pt")),
            criterion=F.nll_loss,
            data_dir=data_dir,
            config=config,
        )
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def test_started(self):
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def test_ended(self):
        print("Test Accuracy:", (self.correct_count / self.total_count).item())

    def val_started(self):
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def val_ended(self):
        print("Val Accuracy:", (self.correct_count / self.total_count).item())

    def run(self, batch: tuple[t.Tensor, t.Tensor]) -> t.Tensor:
        res = self.model(batch.signals)
        _, pred_labels = t.max(res, dim=1)
        self.correct_count += t.sum(pred_labels == batch.labels).cpu()
        self.total_count += t.tensor(len(batch.signals))
        return self.criterion(res, batch.labels)
