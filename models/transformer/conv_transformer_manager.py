import os
import torch as t
import torch.nn.functional as F
from ..models.conv_transformer import ConvTransformer
from ..data_loader import DataLoader
from .seizure_prediction_manager import SeizurePredictionManager


class ConvTransformerManager(SeizurePredictionManager):
    def __init__(
        self,
        *,
        data_dir,
        train_batch_size,
        test_batch_size,
        epochs,
        lr=1e-4,
        device="",
        model_file_path="models_saved",
        gamma=0.90
    ):
        gamma = gamma if gamma != 0 else 0.90
        super().__init__(
            model=ConvTransformer(
                os.path.join(model_file_path, "encoder.pt"),
                seq_length=103,
                embedding_dim=4 * 24,
                num_heads=3,
                depth=3,
                num_labels=2,
            ),
            criterion=F.nll_loss,
            data_dir=data_dir,
            train_batch_size=train_batch_size,
            test_batch_size=test_batch_size,
            epochs=epochs,
            lr=lr,
            device=device,
            model_file_path=model_file_path,
            gamma=gamma,
        )
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)
        print(train_batch_size)

    def test_started(self):
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def test_ended(self):
        print("Test Accuracy:", (self.correct_count / self.total_count).item())

    def val_started(self):
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def val_ended(self):
        print("Val Accuracy:", (self.correct_count / self.total_count).item())

    def run(self, batch: tuple[t.Tensor, t.Tensor]) -> t.Tensor:
        res = self.model(batch.signals)
        _, pred_labels = t.max(res, dim=1)
        self.correct_count += t.sum(pred_labels != batch.labels).cpu()
        self.total_count += len(batch.signals)
        loss = self.criterion(res, batch.labels)
        return loss
