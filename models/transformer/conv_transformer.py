import torch as t
from torch import nn
import torch.nn.functional as F

from .ae import ConvEncoder


class SelfAttention(nn.Module):
    def __init__(self, embedding_dim, heads):
        super().__init__()
        self.embedding_dim = embedding_dim
        self.heads = heads
        self.to_keys = nn.Linear(
            embedding_dim, embedding_dim * heads, bias=False
        )
        self.to_queries = nn.Linear(
            embedding_dim, embedding_dim * heads, bias=False
        )
        self.to_values = nn.Linear(
            embedding_dim, embedding_dim * heads, bias=False
        )
        self.unify_heads = nn.Linear(heads * embedding_dim, embedding_dim)

    def forward(self, x):
        batch_size, tweet_length, embedding_dim = x.size()
        keys = self.to_keys(x).view(
            batch_size, tweet_length, self.heads, embedding_dim
        )
        queries = self.to_queries(x).view(
            batch_size, tweet_length, self.heads, embedding_dim
        )
        values = self.to_values(x).view(
            batch_size, tweet_length, self.heads, embedding_dim
        )
        keys = (
            keys.transpose(1, 2)
            .contiguous()
            .view(batch_size * self.heads, tweet_length, embedding_dim)
        )
        queries = (
            queries.transpose(1, 2)
            .contiguous()
            .view(batch_size * self.heads, tweet_length, embedding_dim)
        )
        values = (
            values.transpose(1, 2)
            .contiguous()
            .view(batch_size * self.heads, tweet_length, embedding_dim)
        )
        queries = queries / (embedding_dim ** (1 / 4))
        keys = keys / (embedding_dim ** (1 / 4))
        dot = F.softmax(t.bmm(queries, keys.transpose(1, 2)), dim=2)
        out = t.bmm(dot, values).view(
            batch_size, self.heads, tweet_length, embedding_dim
        )
        out = (
            out.transpose(1, 2)
            .contiguous()
            .view(batch_size, tweet_length, self.heads * embedding_dim)
        )
        return self.unify_heads(out)


class TransformerBlock(nn.Module):
    def __init__(
        self, embedding_dim, num_heads, *, fc_hidden_multiply=4, dropout=0.4
    ):
        super().__init__()
        self.attention = SelfAttention(embedding_dim, num_heads)
        self.norm1 = nn.LayerNorm(embedding_dim)
        self.norm2 = nn.LayerNorm(embedding_dim)
        self.fc = nn.Sequential(
            nn.Linear(embedding_dim, embedding_dim * fc_hidden_multiply),
            nn.ReLU(),
            nn.Linear(embedding_dim * fc_hidden_multiply, embedding_dim),
        )
        self.droput = nn.Dropout(dropout)

    def forward(self, x):
        attended = self.attention(x)
        x = self.norm1(attended + x)
        x = self.droput(x)
        feedforward = self.fc(x)
        x = self.norm2(feedforward + x)
        x = self.droput(x)
        return x


class ConvTransformer(nn.Module):
    def __init__(
        self,
        encoder_file_path,
        seq_length,
        embedding_dim,
        num_heads,
        depth,
        num_labels,
        output_dropout=0.2,
        block_dropout=0.2,
    ):
        super().__init__()
        self.encoder = ConvEncoder()
        self.encoder.load_state_dict(t.load(encoder_file_path))
        self.embedding_dim = embedding_dim
        self.seq_length = seq_length
        transformer_blocks = []
        for _ in range(depth):
            transformer_blocks.append(
                TransformerBlock(
                    embedding_dim, num_heads, dropout=block_dropout
                )
            )
        self.transformer_blocks = nn.Sequential(*transformer_blocks)
        self.to_probabilities = nn.Linear(embedding_dim, num_labels)
        self.dropout = nn.Dropout(output_dropout)
        self.device = t.device("cuda" if t.cuda.is_available() else "cpu")

    def forward(self, x):
        self.encoder.eval()
        x = self.encoder(x).view(-1, self.seq_length, self.embedding_dim)
        x = self.transformer_blocks(x)
        x = x.max(dim=1)[0]
        x = self.to_probabilities(x)
        return F.log_softmax(x, dim=1)
