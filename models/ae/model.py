import torch as t
from torch import nn
from torch.nn import functional as F


class ConvAE(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.encoder = ConvEncoder(dropout, hidden_size=hidden_size)
        self.decoder = ConvDecoder(dropout, hidden_size=hidden_size)

    def forward(self, x):
        return self.decoder(self.encoder(x))


class ConvEncoder(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.conv1 = nn.Conv3d(23, 23 + 3 * 16, (5, 5, 2))
        self.conv2 = nn.Conv2d(23 + 3 * 16, 23 + 4 * 16, (2, 5))
        self.conv3 = nn.Conv1d(23 + 4 * 16, 23 + 5 * 16, 3)
        self.conv4 = nn.Conv1d(23 + 5 * 16, 23 + 6 * 16, 3)
        self.conv5 = nn.Conv1d(23 + 6 * 16, 23 + 7 * 16, 3)

        self.pool = nn.MaxPool2d(2)
        self.pool1d = nn.MaxPool1d(2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)
        self.batch_norm1 = nn.BatchNorm3d(23)

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = self.batch_norm1(x)
        x = F.relu(self.conv1(x))
        x = t.squeeze(x, dim=4)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv2(x))
        x = x.squeeze(2)
        x = self.dropout_2d(self.pool1d(x))
        x = F.relu(self.conv3(x))
        x = self.dropout_2d(x)
        x = self.pool1d(x)
        x = F.relu(self.conv4(x))
        x = self.dropout_2d(x)
        x = self.pool1d(x)
        x = F.relu(self.conv5(x))
        x = self.dropout_2d(x)
        x = self.pool1d(x)
        return x


class ConvDecoder(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.t_conv5 = nn.ConvTranspose3d(23 + 3 * 16, 23, (2, 8, 2), stride=2)
        self.t_conv4 = nn.ConvTranspose2d(
            23 + 4 * 16, 23 + 3 * 16, (4, 5), stride=2
        )
        self.t_conv3 = nn.ConvTranspose1d(
            23 + 5 * 16, 23 + 4 * 16, 4, stride=2
        )
        self.t_conv2 = nn.ConvTranspose1d(
            23 + 6 * 16, 23 + 5 * 16, 5, stride=2
        )
        self.t_conv1 = nn.ConvTranspose1d(
            23 + 7 * 16, 23 + 6 * 16, 5, stride=2
        )

        self.pool = nn.MaxPool2d(2)
        self.pool1d = nn.MaxPool1d(2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)
        self.batch_norm1 = nn.BatchNorm3d(23)

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = self.dropout_2d(F.relu(self.t_conv1(x)))
        x = self.dropout_2d(F.relu(self.t_conv2(x)))
        x = self.dropout_2d(F.relu(self.t_conv3(x)))
        x = t.unsqueeze(x, 2)
        x = self.dropout(F.relu(self.t_conv4(x)))
        x = t.unsqueeze(x, 4)
        x = self.dropout(F.relu(self.t_conv5(x)))
        return x
