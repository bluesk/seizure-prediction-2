import torch as t
from torch.nn import functional as F
from tqdm import tqdm
from torch.optim.lr_scheduler import StepLR
from torch.utils.tensorboard import SummaryWriter
import ipdb
import os
from pprint import pprint
import matplotlib.pyplot as plt

from ...data_loader import DataLoader

from ..trainer import Trainer, TrainingConfig
from .model import ConvAE, ConvDecoder, ConvEncoder


class AETrainingConfig(TrainingConfig):
    def __init__(self):
        super().__init__()
        self.gamma = 0.85


class AETrainer(Trainer):
    def __init__(self, data_dir: str, config: AETrainingConfig):
        super().__init__(
            model=ConvAE(dropout=dropout),
            criterion=t.nn.MSELoss(),
            data_dir=data_dir,
            config=config,
        )

    def test_ended(self):
        t.save(
            self.model.encoder.state_dict(),
            os.path.join(self.model_file_path, "encoder.pt"),
        )
        t.save(
            self.model.decoder.state_dict(),
            os.path.join(self.model_file_path, "decoder.pt"),
        )

    def run(self, batch):
        reconstructed = self.model(batch.signals)
        loss = self.criterion(batch.signals, reconstructed)
        return loss
