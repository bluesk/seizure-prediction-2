import torch as t
from torch import nn
from torch.nn import functional as F
import ipdb


class ConvAE(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.encoder = ConvEncoder(dropout, hidden_size=hidden_size)
        self.decoder = ConvDecoder(dropout, hidden_size=hidden_size)

    def forward(self, x):
        return self.decoder(self.encoder(x))


class ConvEncoder(nn.Module):
    def __init__(
        self, channels: int = 19, dropout: float = 0.2, hidden_size: int = 1000
    ):
        super().__init__()
        self.conv1 = nn.Conv3d(channels, channels + 3 * 16, (5, 5, 2))
        self.conv2 = nn.Conv2d(
            channels + 3 * channels, channels + 4 * 16, (2, 5)
        )
        self.conv3 = nn.Conv1d(channels + 4 * 16, channels + 5 * 16, 3)

        self.pool = nn.MaxPool2d(2)
        self.pool1d = nn.MaxPool1d(2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)
        self.batch_norm1 = nn.BatchNorm3d(channels)

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = self.batch_norm1(x)
        x = F.relu(self.conv1(x))
        x = t.squeeze(x, dim=4)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv2(x))
        x = x.squeeze(2)
        x = self.dropout_2d(self.pool1d(x))
        x = F.relu(self.conv3(x))
        x = self.dropout_2d(x)
        return x


class ConvDecoder(nn.Module):
    def __init__(
        self, channels: int = 23, dropout: float = 0.2, hidden_size: int = 1000
    ):
        super().__init__()
        self.t_conv1 = nn.ConvTranspose1d(
            channels + 5 * 16, channels + 4 * 16, 3
        )
        self.t_conv2 = nn.ConvTranspose2d(
            channels + 4 * 16, channels + 3 * 16, (2, 6), stride=2
        )
        self.t_conv3 = nn.ConvTranspose3d(
            channels + 3 * 16, channels, (6, 6, 2), stride=2
        )
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = F.relu(self.t_conv1(x))
        x = self.dropout_2d(x)
        x = x.unsqueeze(2)
        x = F.relu(self.t_conv2(x))
        x = self.dropout_2d(x)
        x = t.unsqueeze(x, dim=4)
        x = F.relu(self.t_conv3(x))
        x = self.dropout_2d(x)
        return x
