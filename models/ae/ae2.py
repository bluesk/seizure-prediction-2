import torch as t
from torch import nn
from torch.nn import functional as F
import ipdb


class ConvAE(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.encoder = ConvEncoder(dropout, hidden_size=hidden_size)
        self.decoder = ConvDecoder(dropout, hidden_size=hidden_size)

    def forward(self, long_x):
        xs = t.split(long_x, 1, dim=2)
        rec_xs = tuple(
            self.decoder(self.encoder(x.squeeze(dim=2))).unsqueeze(dim=2)
            for x in xs
        )
        rec_long_x = t.cat(rec_xs, dim=2)
        return rec_long_x


class ConvEncoder(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.conv1 = nn.Conv2d(23, 23 + 3 * 16, (5, 2))
        self.conv2 = nn.Conv1d(23 + 3 * 16, 23 + 4 * 16, 3)
        self.conv3 = nn.Conv1d(23 + 4 * 16, 23 + 5 * 16, 3)

        self.pool = nn.MaxPool2d(2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)
        self.batch_norm1 = nn.BatchNorm2d(23)

    def forward(self, x: t.Tensor) -> t.Tensor:
        print(x.shape)
        x = self.batch_norm1(x)
        x = F.relu(self.conv1(x))
        x = t.squeeze(x, dim=3)
        print(x.shape)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv2(x))
        print(x.shape)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv3(x))
        x = self.dropout_2d(x)
        return x


class ConvDecoder(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.t_conv1 = nn.ConvTranspose2d(23 + 5 * 16, 23 + 4 * 16, 3)
        self.t_conv2 = nn.ConvTranspose2d(
            23 + 4 * 16, 23 + 3 * 16, (5, 4), stride=2
        )
        self.t_conv3 = nn.ConvTranspose3d(23 + 3 * 16, 23, (6, 7, 2), stride=2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = F.relu(self.t_conv1(x))
        x = self.dropout_2d(x)
        x = F.relu(self.t_conv2(x))
        x = self.dropout_2d(x)
        x = t.unsqueeze(x, dim=3)
        x = F.relu(self.t_conv3(x))
        x = self.dropout_2d(x)
        return x
