import torch as t

from torch.nn import functional as F
from tqdm import tqdm
from torch.optim.lr_scheduler import StepLR
from torch.utils.tensorboard import SummaryWriter
import ipdb
import os
from pprint import pprint
import matplotlib.pyplot as plt

from .model import Classifier
from ..trainer import Trainer, TrainingConfig
from ...data_loader import DataLoader


class ClassifierTrainingConfig(TrainingConfig):
    def __init__(self):
        super().__init__()
        self.gamma = 0.8


class ClassifierTrainer(Trainer):
    def __init__(self, data_dir: str, config: ClassifierTrainingConfig):
        super().__init__(
            model=Classifier(),
            criterion=F.nll_loss,
            data_dir=data_dir,
            config=config,
        )
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def test_started(self):
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def test_ended(self):
        print("Test Accuracy:", (self.correct_count / self.total_count).item())

    def val_started(self):
        self.correct_count = t.tensor(0)
        self.total_count = t.tensor(0)

    def val_ended(self):
        print("Val Accuracy:", (self.correct_count / self.total_count).item())

    def run(self, batch: tuple[t.Tensor, t.Tensor]) -> t.Tensor:
        res = self.model(batch.signals)
        _, pred_labels = t.max(res, dim=1)
        self.correct_count += t.sum(pred_labels == batch.labels).cpu()
        self.total_count += len(batch.signals)
        # print(res)
        # print(batch.labels)
        loss = self.criterion(res, batch.labels)
        return loss
