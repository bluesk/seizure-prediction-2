import torch as t
from torch import nn
from torch.nn import functional as F

# from .mini_conv_ae import ConvEncoder
import ipdb


class Classifier(nn.Module):
    def __init__(self, dropout: float = 0.2, hidden_size: int = 1000):
        super().__init__()
        self.conv1 = nn.Conv3d(23, 23 + 3 * 16, (5, 5, 2))
        self.conv2 = nn.Conv2d(23 + 3 * 16, 23 + 4 * 16, (2, 5))
        self.conv3 = nn.Conv1d(23 + 4 * 16, 23 + 5 * 16, 3)
        self.conv4 = nn.Conv1d(23 + 5 * 16, 23 + 6 * 16, 3)
        self.conv5 = nn.Conv1d(23 + 6 * 16, 23 + 7 * 16, 3)

        self.pool = nn.MaxPool2d(2)
        self.pool1d = nn.MaxPool1d(2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)
        self.batch_norm1 = nn.BatchNorm3d(23)
        self.fc1 = nn.Linear(2565, 600)
        self.fc2 = nn.Linear(600, 60)
        self.fc3 = nn.Linear(60, 2)

    def forward(self, x: t.Tensor) -> t.Tensor:
        x = self.batch_norm1(x)
        x = F.relu(self.conv1(x))
        x = t.squeeze(x, dim=4)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv2(x))
        x = x.squeeze(2)
        x = self.dropout_2d(self.pool1d(x))
        x = F.relu(self.conv3(x))
        x = self.dropout_2d(x)
        x = self.pool1d(x)
        x = F.relu(self.conv4(x))
        x = self.dropout_2d(x)
        x = self.pool1d(x)
        x = F.relu(self.conv5(x))
        x = self.dropout_2d(x)
        x = self.pool1d(x)

        x = t.flatten(x, 1)
        x = self.dropout(F.relu(self.fc1(x)))
        x = self.dropout(F.relu(self.fc2(x)))
        x = self.dropout(F.relu(self.fc3(x)))
        return F.log_softmax(x, dim=1)


"""
class ConvClassifier(nn.Module):
    def __init__(self, dropout: float = 0.2):
        super().__init__()
        hidden_size = 100
        n_elements = 1704
        self.conv1 = nn.Conv3d(23, 23 + 16, (5, 5, 2))
        self.conv2 = nn.Conv2d(23 + 16, 23 + 2 * 16, 3)
        self.conv3 = nn.Conv2d(23 + 2 * 16, 23 + 3 * 16, 3)

        self.pool = nn.MaxPool2d(2)
        self.dropout_2d = nn.Dropout2d(dropout)
        self.dropout = nn.Dropout(dropout)
        self.fc1 = nn.Linear(n_elements, hidden_size)
        self.fc2 = nn.Linear(hidden_size, 2)
        self.batch_norm1 = nn.BatchNorm3d(23)

    def forward(self, x):
        print(x.shape)
        x = self.batch_norm1(x)
        x = F.relu(self.conv1(x))
        x = t.squeeze(x, dim=4)
        print(x.shape)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv2(x))
        print(x.shape)
        x = self.dropout_2d(self.pool(x))
        x = F.relu(self.conv3(x))
        x = self.dropout_2d(self.pool(x))
        x = t.flatten(x, 1)
        x = self.dropout(F.relu(self.fc1(x)))
        x = self.dropout(F.relu(self.fc2(x)))
        x = F.log_softmax(x, dim=1)
        return x
"""
