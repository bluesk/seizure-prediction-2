from types import prepare_class
import torch as t
import os
from .preprocess.output_classes import PreprocessedData
import numpy as np
import pickle
import threading


class CachedLoader:
    def __init__(
        self, files: list[str], batch_size: int, subsegments_number: int
    ):
        self.__length = int(np.ceil(subsegments_number / batch_size))
        self.batch_size = batch_size
        self.files = files
        self.__file_index = 0
        self.__next_batches = None
        self.__current_batches = None
        self.__loader = threading.Thread(target=self.__load_next)

    def __load_next(self):
        with open(self.files[self.__file_index], "rb") as f:
            self.__next_batches = self._batchify(pickle.load(f))
        self.__file_index += 1

    def __len__(self):
        return self.__length

    def __iter__(self):
        return self

    def _batchify(self, segments: tuple[t.Tensor, t.Tensor]):
        batches = []
        for label, signal in segments:
            batches.append(
                (
                    t.split(label, self.batch_size),
                    t.split(signal, self.batch_size),
                )
            )
        return batches

    def __next__(self):
        if self.__file_index == len(self.files) - 1:
            raise StopIteration
        if self.__current_batches is None:
            self.__loader.start()
            self.__loader.join()
            self.__current_batches = self.__next_batches
            self.__loader.start()
        if len(self.__current_batches) == 0:
            if self.__next_batches == None:
                self.__loader.join()
            self.__current_batches = self.__next_batches
            self.__loader.start()

        result = self.__current_batches[0]
        self.__current_batches = self.__current_batches[1:]
        return result


class DataLoader:
    def train_test_split_subjects(self):
        subjects = np.array(self.preprocessed_data.subjects)
        subjects.shuffle()
        self.train_subjects = subjects[: int(len(subjects) * self.train_ratio)]
        self.test_subjects = subjects[
            int(len(subjects) * (1 - self.train_ratio)) :
        ]

        def contains(name: str, names: list[str]):
            return len([n for n in names if n in name]) > 0

        self.train_files = [
            fn
            for fn in self.preprocessed_data.files
            if contains(fn, self.train_subjects)
        ]
        self.test_files = [
            fn
            for fn in self.preprocessed_data.files
            if contains(fn, self.test_subjects)
        ]

    def train_test_split(self):
        self.train_files = self.preprocessed_data.files[
            : int(len(self.preprocessed_data.files) * self.train_ratio)
        ]
        self.test_files = self.preprocessed_data.files[
            int(len(self.preprocessed_data.files) * (1 - self.train_ratio)) :
        ]

    def __init__(
        self,
        storage_directory: str,
        train_ratio: float,
        train_batch_size: int,
        test_batch_size: int,
        test_with_separate_subjects=True,
    ):
        assert 0 < train_ratio <= 1.0, "Train ratio must be between 1.0 and 0"
        self.train_ratio = train_ratio
        self.preprocessed_data = PreprocessedData.from_folder(
            storage_directory
        )
        self.train_subjects = []
        self.test_subjects = []
        self.train_files = []
        self.test_files = []
        if test_with_separate_subjects:
            self.train_test_split_subjects()
        else:
            self.train_test_split()
        self.train_loader = CachedLoader(
            self.train_files,
            train_batch_size,
            int(self.preprocessed_data.subsegments_count * train_ratio),
        )
        self.test_loader = CachedLoader(
            self.test_files,
            test_batch_size,
            int(self.preprocessed_data.subsegments_count * (1 - train_ratio)),
        )
