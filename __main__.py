from argparse import ArgumentParser
from pprint import pprint
import json
import os

from .models.classifier.trainer import ClassifierTrainer
from .models.ae.trainer import AETrainer
from .models.lstm.trainer import ConvLSTMTrainer
from .config import Config
from .preprocess.preprocess import preprocess


def main():
    parser = ArgumentParser()

    parser.add_argument(
        "action",
        choices=["generate-config", "preprocess", "train", "plot"],
        type=str,
    )
    parser.add_argument(
        '--config',
        type=str,
        default='config.json',
        help='Location of the configuration file'
    )
    parser.add_argument(
        "--model-type",
        "-mt",
        choices=["classifier", "ae", "lstm", "transformer"],
        type=str,
        default="ae",
    )
    args = parser.parse_args()
    if args.action == "generate-config":
        config = Config()
        config.write_json(args.config)
    else:
        assert os.path.exists(
            args.config
        ), 'Configuration file not found. You can generate a default one with the "generate-config" option'
        config = Config(args.config)
    if args.action == "preprocess":
        preprocess(args.config, config)
    elif args.action == "train":
        if args.model_type == "classifier":
            smm = ClassifierTrainer(
                data_dir=config.data_dir, config=config.classifier
            )
        elif args.model_type == "ae":
            smm = AETrainer(data_dir=config.data_dir, config=config.ae)
        elif args.model_type == "lstm":
            smm = ConvLSTMTrainer(data_dir=config.data_dir, config=config.lstm)
        if args.action == "train":
            smm.train()


if __name__ == "__main__":
    main()
